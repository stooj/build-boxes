#!/bin/bash
# shellcheck disable=SC2015

# Install vanilla Arch with a simple partition layout and UEFI
# Suitable for a virtual machine with a small disk
# by Stoo Johnston
#
# Heavily inspired by the following projects:
# - Arch Linux Easy ZFS (ALEZ)
#   by Dan MacDonald with contributions from John Ramsden
# - Chris Campbell (https://gist.github.com/xunil154)

# To mount in a VM:

# https://xapax.github.io/blog/2017/05/09/sharing-files-kvm.html
# mkdir /installers
# mount -t 9p -o trans=virtio /installation_scripts /installers

# Exit on error
set -o errexit -o errtrace

# Set a default locale during install to avoid mandb error when indexing man pages
export LANG=C

version=0.1

installdir="/mnt"

if [ $# -ne 2 ]; then
    echo "Usage: $0 <device> <system_name>"
    echo "Where <device> is sdX"
    echo "Where <system_name> is the desired hostname (including domain if you want)"
    echo "Example: $0 sda mymachine.example.com"
    echo "do NOT use the /dev/disk/by-id/.... the script will find that automatically"
    exit 1
fi

ee() {
    # Error and exit function
    if [ "$1" -ne 0 ] ; then
        echo "[ERROR] $2 "
        exit "$1"
    fi
}

check_internet() {
    ping -c 1 archlinux.org &> /dev/null || return 1
    return 0
}
# Run stuff in the Arch chroot install function with optional message
chrun() {
    [[ -n "${2}" ]] && echo "${2}"
    arch-chroot "${installdir}" /bin/bash -c "${1}"
}

connected=1
check_internet && connected=0

if [ ${connected} -eq 1 ] ; then
    ee 1 "You are not connected to the internet."
fi

SYSTEM_FQDN="$2"
SYSTEM_NAME=${SYSTEM_FQDN%%.*} # Hostname

echo "############# Disk Configuration #################"

userdev=$(basename "$1")

DISK=${userdev}
DISKDEV="/dev/${DISK}"
if [ -z "${DISKDEV}" ] ; then
    ee 1 "Could not lookup disk by id... are you sure '${userdev}' is correct?"
fi

echo "Installing vanilla Arch suitable for a VM (installer version ${version})"
echo "USING DISK: ${DISKDEV}"
echo "If this is wrong, press Ctrl^C within 15 seconds"
sleep 5
echo "10s"
sleep 5
echo "5s"
sleep 5

echo "Destroying existing partitions"

parted ${DISKDEV} print
parted --script ${DISKDEV} mklabel msdos
parted --script ${DISKDEV} mkpart primary ext4 0% 100%
parted --script ${DISKDEV} toggle 1 esp
parted ${DISKDEV} print

echo "Partitions created, reloading table on ${DISKDEV}"
partprobe ${DISKDEV}
sleep 2

SYS="${DISK}1"

SYSDEV=/dev/${SYS}

echo "Formatting partitions"

mkfs.ext4 ${SYSDEV}
ee $? "Failed to format sys partition ${SYSDEV}"

echo "Mounting partitions"
mount ${SYSDEV} ${installdir}

echo "Installing base packages"
pacstrap ${installdir} base linux linux-firmware

echo "Generating fstab"
genfstab -U ${installdir} >> ${installdir}/etc/fstab

chrun "ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime" "Setting timezone to UK"
chrun "hwclock --systohc" "Running hwclock"
chrun "pacman --noconfirm -Sy vim" "Installing vim"
chrun "sed -i -e 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen" "Enabling US_UTF"
chrun "sed -i -e 's/#en_GB.UTF-8 UTF-8/en_GB.UTF-8 UTF-8/' /etc/locale.gen" "Enabling GB_UTF"
chrun "locale-gen" "Generated locales"
chrun "echo 'LANG=en_GB.UTF-8' >> /etc/locale.conf" "Setting LANG var in locale.conf"
chrun "echo $SYSTEM_FQDN > /etc/hostname" "Setting hostname"
chrun "echo '127.0.0.1 localhost' > /etc/hosts" "Setting ipv4 localhost"
chrun "echo '::1       localhost' >> /etc/hosts" "Setting ipv6 localhost"

if [ ${SYSTEM_FQDN} != ${SYSTEM_NAME} ] ; then
    chrun "echo "127.0.0.1 ${SYSTEM_FQDN} ${SYSTEM_NAME}.localdomain ${SYSTEM_NAME}" >> /etc/hosts" "Adding loopback to hosts"
else
    chrun "echo "127.0.0.1 ${SYSTEM_NAME}.localdomain ${SYSTEM_NAME}" >> /etc/hosts" "Adding loopback to hosts"
fi

chrun "pacman --noconfirm -Sy networkmanager" "Installing networkmanager"
chrun "systemctl enable NetworkManager.service" "Enabling NetworkManager"

chrun "pacman --noconfirm -Sy grub" "Installing boot tools"
chrun "grub-install --target=i386-pc ${DISKDEV}" "Running grub-install"
chrun "grub-mkconfig -o /boot/grub/grub.cfg" "Creating grub config"

chrun "passwd" "Setting the root password"

exit 1
echo "Unmounting partitions"
umount -R ${installdir}

echo "Installation complete. You can reboot now"

#!/bin/bash

# Exit on error
set -o errexit -o errtrace

# Set a default locale during install to avoid mandb error when indexing man pages
export LANG=C

# Fix grub's "failed to get canonical path" error
export ZPOOL_VDEV_NAME_PATH=1

version=0.1

installdir="/mnt"
archzfs_pgp_key="F75D9D76"
zroot="zroot"


